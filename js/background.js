// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Called when the url of a tab changes.
function checkForValidUrl(tabIdParam, changeInfo, tab) {
    // For now everything will do.
    var url = tab.url;
    if (url.indexOf("chrome://") < 0) {
        chrome.pageAction.show(tabIdParam);
    }
}

chrome.tabs.onUpdated.addListener(checkForValidUrl);
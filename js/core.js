function initCurteEu() {

    function sError() {
        $("*").css("cursor", "default");
        $("#url").val("Sem resposta!");
    }

    function gClipboard(id) {
        document.getElementById(id).select();
        document.execCommand("Copy");
    }

    var data = {
        'url': document.location.href,
        'originalUrl': document.location.href,
        'title': document.title
    };
    // Google Maps URL override
    var link = document.getElementById('link');
    if (link && link.href) {
        data.url = link.href;
    }
    chrome.extension.connect().postMessage(data);

    $(document).ready(function() {
        $("*").css("cursor", "wait");
        try {
            chrome.tabs.getSelected(null, function(tab) {
                var url = tab.url;
                $.ajax({
                    url: 'http://curte.eu/',
                    data: {
                        "url": url,
                        "f": "json"
                    },
                    success: function(data) {
                        data = json_decode(data);
                        if (data != undefined) {
                            var shorturl = data.result.shorturl;
                            if (shorturl.indexOf("http://curte.eu/") > -1) {
                                if (data.result.shorturl != "http://curte.eu/") {
                                    var title = data.result.title;
                                    $("#url").val(shorturl);
                                    $("*").css("cursor", "default");
                                    $("#url").css("cursor", "text");
                                    $(".disabled").removeClass("disabled");
                                    $("#share_twitter").click(function() {
                                        //var titulo = encodeURI("Eu curti! ") + encodeURI(shorturl);
                                        var url = "https://twitter.com/intent/tweet?hashtags=CurteEu&source=tweetbutton&text=" + encodeURI("Eu curti!") + "&url=" + encodeURI(shorturl); //    &original_referer=https%3A%2F%2Ftwitter.com%2Fabout%2Fresources%2Fbuttons
                                        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
                                    });
                                    $("#share_facebook").click(function() {
                                        var url = "http://www.facebook.com/sharer.php?u=" + encodeURI(shorturl);
                                        window.open(url, "", "toolbar=0, status=0, width=650, height=450");
                                    });
                                    $("#share_google").click(function() {
                                        var url = "https://plus.google.com/share?url=" + encodeURI(shorturl);
                                        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
                                    });
                                    $("#copy2clipboard").click(function() {
                                        gClipboard("url");
                                    });
                                    $('#qrcode').qrcode({width: 128, height: 128, text: shorturl});
                                    return true;
                                }
                            }
                        }
                        sError();
                    },
                    error: function() {
                        sError();
                    }
                });
            });
        } catch (e) {
            sError();
        }
    });
}

document.addEventListener('DOMContentLoaded', function() {
    initCurteEu();
});